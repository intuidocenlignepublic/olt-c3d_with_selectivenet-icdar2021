# Code for publication of ICDAR 2021
## Online Spatio-Temporal 3D Convolutional Neural Network for Early Recognition of Handwritten Gestures.
### William Mocaër, Eric Anquetil, Richard Kulpa

The code is written in python with tensorflow 2.1.

## Model
The package "Model" contains the model file ModelEarly2D.py

## FittedModel
The package "FittedModel" contains the fitted model, containing weights, results on Test set and informations about weights

## utils
Utils contains a parser for inkml partially developped by Robin Dahuron.

## Databases
Each folder of database contains three files : 
- Preprocessing - translate the online input signal into image sequences
- Training - fit to optimize the losses
- Evaluation_OnVal - the evaluation runs on validation set
- Evaluation - the evaluation runs on test set
- VisualizingResult - export the results in image, as in qualitative results of the paper

The databases ILGDB and MTGSetB should be available on the intuidoc website, 
if not you can contact Eric Anquetil or William Mocaër.
### ILGDB
ILGDB. The ILG database is a mono-stroke pen-based gestures dataset performed by 38 users. It contains 21 different gesture classes with a total of 1923 samples, 693 are used for training and 1230 for testing.

N. Renau-Ferrer, P. Li, A. Delaye and E. Anquetil, "The ILGDB database of realistic pen-based gestural commands," Proceedings of the 21st International Conference on Pattern Recognition (ICPR2012), Tsukuba, 2012, pp. 3741-3744.
### MTGSetB
The MTGSetB dataset is composed of 45 dierent multi-touch gestures regrouped into 31 rotation invariant gesture classes made by 33 users.

Zhaoxin Chen, Eric Anquetil, Harold Mouchère, Christian Viard-Gaudin. A graph modeling strategy for multi-touch gesture recognition. 14th International Conference on Frontiers in Handwriting Recognition (ICFHR-2014), Sep 2014, Crete island, Greece


### NicIcon
A set of 14 icons that are important in the domain of crisis management and incident response systems was selected. The icons were designed such that
- they have a visual resemblance to the objects they represent or correspond to well known corresponding symbols (so that they are easy to learn by the users)
- are distinguishable by the computer.

Niels, Ralph & Willems, Don & Vuurpijl, Louis. (2009). The NicIcon Database of Handwritten Icons for Crisis Management. 

The experimentations with our model were done quickly, and not very explored, so results were not reported in the paper.


### requirement.txt
this file contains all dependencies necessary to run the code


### FAQ

> est-ce que l'utilisation de wandb n'alourdi pas les log sachant qu'il y a Tensorboard ? sur igrida il y a des manip à faire pour utiliser wandb ?
- wandb c'est un outil comme tensorboard, ça va pas t'afficher des trucs dans la console mais sur un tableau de bord, avec des outils en plus, c'est vraiment une alternative a tensorboard
- pas particulièrement de manip, juste installer la librairie sur l'environnement et se logger avec une commande

> quel script utilises-tu pour lancer un train sur igrida ?
- j'utilise training.py, plus facile à lancer qu'un notebook, mais il faut lancer préalablement le préprocessing pour générer les fichiers prépocessed

> en moyenne combien met une epochs à se faire sur ILGDB (CPU et/ou GPU) ?
- 43 second pour la première epoch (toujours plus long) puis 16s pour les autres (sur abacus 13, rtx2080ti)

> concernant l'augmentation de données (ILGDB), cela aide beaucoup ?
- pour l'augmentation, cela aide beaucoup sur ILGDB parce que l'ensemble de test est très différent de l'ensemble de train en termes de tailles de symboles,

> pourquoi il y a un -0.1 pour les données de tests ? (dans preprocessing sur la taille scale)
- pour le -0.1 c'est que j'ai réduit a priori la taille des données de test, cela correspond au final à ce qui est mis dans le papier, au final j'aurai du retirer 0.1 dans le fichier de config pour et retirer le -0.1 dans le code, mais ça revient au même

> a quoi servent les instructions #%%md et #%% ?
- c'est les instructions pour les notebooks jupyter, %%md pour faire un affichage de texte et %% pour le code. Si il y en a dans les fichiers .py c'est jsute que j'ai copier coller le code des notebooks pour le mettre dans le .py

---

> J'ai été embêté avec les links des différents modules (Model) et été obligé de modifier le PYTHONPATH, chose que je ne faisais pas avant
- ah, oui, c'est que moi j'avais un fichier "main" à la racine qui import le training (import ILGDB.Training), en faisant ça ta pas besoin de jouer avec le pythonpath il me semble

> Vers combien d'epoch le modèle converge en général sur cette base ?
- ça overfit a un moment, je pense que pour ILG il faut pas trop aller au delà de 200 epochs, à 100 epcohs ta réjà un truc pas mal, mais de préférence il faut utiliser l'early stopping

> Concernant les résultats décrit dans ton article (table 3), cela correspond au "val_output_1_TAR_allValues" pour le TAR par exemple (*100 pour les pourcentages) ?
- Non, pour avoir les résultats il faut lancer le script évaluation parce que c'est assez spécial, je prend que la première valeur acceptée à chaque fois, les metrics dans l'apprentissage donne un résultat indicatif mais c'est pas la même métric qu'on utilise pour mesurer

---

> Quelle impact à la taille des images d'entrée (30 ou 40 px) : plus c'est grand plus ça permet de "contenir" le geste sans qu'il dépasse, donc c'est à voir avec les dimensions min/max des données d'entrée (avec la remise à l'échelle) ?
- Oui c'est plus ou moins l'idée, mais la taille en soit n'est pas un problème puisque l'image "glisse" avec le déplacement, de sorte à toujours voir le dernier morceau du geste. Mais, si il y a plusieurs doigts en même temps à deux endroits opposés de l'image, on ne peut pas glisser d'un côté sans cacher l'autre doigts, donc pour essayer de capter deux doigts a l'opposer ya deux solutions :  1) réduire l'echelle, mais on vuet quand même garder un niveau de résolution raisonnable, 2) agrandir l'image, après c'est possible qu'il y ait encore des débordements (ne pas voir le nouveau tracé si deux doigts sont à l'opposés) avec 40px, mais en tout cas moins qu'avec 30 à la même résolution.

> Quel impact à les param couverture, lambda ainsi que le coeff pour les 2 loss ?
- la couverture définit le pourcentage de rejet cible, par exemple rejeter 50% des trames. lambda (les modèles ont bien été appris avec lambda=32, mais c'est pas utilisé dans la phase de test de toute façon) définit le poids qu'on apporte au respect de ce pourcentage dans la loss. Donc si tu n'as pas besoin de rejet ta pas besoin de cette partie là de la loss, en fait tu n'as pas besoin de toute la première loss.

> Comment configurer l'impact/importance du rejet (en gros pour avoir un rejet fort ou faible) ?
- Via la couverture et lambda justement, (surtout la couverture en pratique), sachant que là on parle de rejet temporel (les trames sont rejetés ou acceptés). Le problème de la couverture c'est que à cause de la loss ça à tendance à dévier pour faire moins le moins de rejet possible en optimisant les bonnes classifications (du coup à force de déviation la couverture observée peut augmenter). J'ai une idée de correction pour ça donc si jamais t'a un vrai problème avec ça je pourrai t'en parler

> Concernant les param du genre denseSize(300), learning rate, nbFeatureMap, c'est des valeurs à configurer en fonction du jeu de données et/ou bien des valeurs qui fonctionnent bien en générale ?
- Je pense que c'est à optimiser en fonction du jeu de données, après ça reste dans le même ordre de grandeur si c'estle même type de données j'imagine. Pour le learning rate je ne le tune pas beaucoup en général, avec Adam on peut moins s'en soucier j'ai l'impression.

---

> pour la couverture, 1.0 = 100% de couverture, c'est-à-dire 100% d'acceptation ou plutôt on cible 100% de rejet ?
- couverture = 1 => on vise 100% d'acceptation donc 0% de rejet

> Pour faire un premier test sans ou avec peu de rejet, je peux utiliser que la loss 'lossHAux' et donc prendre une valeur très petite voir zéro pour le param weightLoss1 ? par conséquent la loss 'lossFGWithReject' n'est plus utilisé et la couverture et le lambda n'ont pas d'impact
- oui tu peux faire ça, tu peux aussi régler la couverture avec une grande valeur (genre 1)
