#%% md

# Training model

#%%
import sys
separator = "/"
sys.path.insert(1,separator.join(sys.path[0].split(separator)[:-1]))
import os
import random
from datetime import datetime
from typing import List
import wandb
from wandb.keras import WandbCallback
import tensorflow as tf
import numpy as np
from Model.ModelEarly2D import ModelEarly2D, ModeTestChannel

pathDB = "/srv/tempdd/wmocaer/data/2D/MTG/MTGSetB/"
pathPreprocessedData = pathDB+"PreprocessedData"+separator
pathLog =pathDB+"Log"+separator
# physical_devices = tf.config.experimental.list_physical_devices('GPU')
# assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
#
# config = tf.config.experimental.set_memory_growth(physical_devices[0], True)

f = open("MTGSetB/config.txt","r")
configParams = f.readlines()
f.close()
configParams = eval("\n".join(configParams))

multiplierCoord = configParams["multiplierCoord"]
thresholdCuDi = configParams["thresholdCuDi"]# value in pixel
dimensionsImage,canal = np.array(configParams["dimensionsOutputImage"]),2
dataset="MTGSetB"

#%% md

### Define loss and metrics

#%%

couverture = 0.75
nbClass=31
def getWo0TruePred(y_true,y_pred):
    #y_true: [batch,nbSeg,nbClass]
    #y_pred: [batch,nbSeg,nbClass]
    y_true = tf.reshape(tf.repeat(y_true[:,0,:],tf.shape(y_true)[1],axis=0),
                        [-1,tf.shape(y_true)[1],nbClass])
    originalCount = tf.cast(tf.shape(y_true)[0],tf.float32)*tf.cast(tf.shape(y_true)[1],tf.float32)
    #select the accepted predictions
    y_pred_accepted_mask = tf.greater(y_pred[:,:,0],0.5) # y_pred[:,0] correspond to the selective gate of each elem
    # tf.print("shape y_pred_accepted_mask",tf.shape(y_pred_accepted_mask))
    # print("shape y_pred_accepted_mask",y_pred_accepted_mask)
    # print("shape y_true",y_true)
    # tf.print("shape y_true",tf.shape(y_true))
    #mask : [batch,seq]
    y_predWo0 = y_pred[:,:,1:]
    y_predWo0_accepted = tf.boolean_mask(y_predWo0,y_pred_accepted_mask,axis=0) # don't take the prediction rejected
    y_trueWo0_accepted = tf.boolean_mask(y_true,y_pred_accepted_mask,axis=0) # [batch*seg,nbClass]
    # tf.print("shape y_trueWo0_accepted",tf.shape(y_trueWo0_accepted))
    # print("shape y_trueWo0_accepted",y_trueWo0_accepted)
    newCount = tf.cast(tf.shape(y_trueWo0_accepted)[0],tf.float32)
    return y_trueWo0_accepted,y_predWo0_accepted,originalCount,newCount

catCroEnt = tf.keras.losses.CategoricalCrossentropy(reduction=tf.keras.losses.Reduction.SUM_OVER_BATCH_SIZE)
def lossFGWithReject(y_true,y_pred,lambdaHyper):
    #y_true: [batch,nbSeg,nbClass]
    #y_pred: [batch,nbSeg,nbClass]
    y_true = tf.reshape(tf.repeat(y_true[:,0,:],tf.shape(y_true)[1],axis=0),
                        [-1,tf.shape(y_true)[1],nbClass])
    loss = catCroEnt(
        tf.repeat(y_pred[:,:,:1],nbClass,axis=2) *y_true , # g(x)*ytrue * log(pred)
        y_pred[:,:,1:])
    loss += lambdaHyper*tf.maximum(couverture-tf.reduce_mean(y_pred[:,:,0]),0)**2
    return loss

def lossHAux(y_true,y_pred):
    loss = catCroEnt(y_true,y_pred)
    return loss

def TAR_allValues(y_true,y_pred):
    y_true, y_pred, originalCount, newCount = getWo0TruePred(y_true,y_pred)
    return tf.metrics.categorical_accuracy(y_true,y_pred)*newCount/originalCount

def FAR_allValues(y_true,y_pred):
    y_true, y_pred, originalCount, newCount = getWo0TruePred(y_true,y_pred)
    return (1-tf.metrics.categorical_accuracy(y_true,y_pred))*newCount/originalCount
def RejectRate_allValues(y_true,y_pred):
    y_true, y_pred, originalCount, newCount = getWo0TruePred(y_true,y_pred)
    return (originalCount-newCount)/originalCount

#%%

trainPart = 0.85

trainFiles = os.listdir(pathPreprocessedData+"Train")

nbTrain = int(trainPart*len(trainFiles))
nbValid = len(trainFiles)-nbTrain
train = trainFiles[0:nbTrain]
valid = trainFiles[nbTrain:]
random.shuffle(train)

#%% md

### Define hyper-parameters

#%%

dilatationRates = [1, 2, 4, 8, 16, 1, 2, 4, 8, 16]
config = {    "multiplierCoord":multiplierCoord,
              "treshCudi":thresholdCuDi,
                "dimension":[dimensionsImage[0],dimensionsImage[1]],
              "batchSize": 40,
              "lambdahyper": 32,
                "couverture":couverture,
              "weightLoss1":0.5,
              "learning_rate":0.003,
               "doGlu":False,
               "dropoutVal":0.1,
               "denseSize":300,
               "denseDropout":0.3,
               "nbFeatureMap":25,
               "dilatationRates":dilatationRates,
               "maxPoolSpatial":True,
               "poolSize":(1,3,3),
                "nbDenseLayer":1,
               "train_size":nbTrain,
               "val_size":nbValid,
              "modeChannel":ModeTestChannel.BOTH
               }


#%% md

### Initialize wandb

#%%

tags = [dataset]
wandBRun = wandb.init(project="precoce2d-deep-wavenet",entity="intuidoc-gesture-reco",save_code=True,reinit=True,tags=tags,config=config)
config = wandb.config # will set new hyperparameters when sweep used
wandb.summary["dataset"]=dataset
wandb.summary["canaux"]=canal
wandb.summary["nbClass"]=nbClass
wandb.summary["receptiveField"]=sum(config.dilatationRates)
wandb.summary["nbConvLayers"]=len(config.dilatationRates)

print("WANDB run name = "+ wandBRun.name)
wandBRunDir = wandBRun.dir

#%% md

### Define the model

#%%

metrics = [TAR_allValues,FAR_allValues,RejectRate_allValues]
model = ModelEarly2D(nbClass=nbClass,boxSize=(dimensionsImage[0],dimensionsImage[1],canal),
                     doGLU=config.doGlu,dropoutVal=config.dropoutVal,denseNeurones=config.denseSize,
                     denseDropout=config.denseDropout,nbFeatureMap=config.nbFeatureMap,
                     dilatationsRates=config.dilatationRates,maxPoolSpatial=config.maxPoolSpatial,
                     poolSize=config.poolSize,poolStrides=config.poolSize,modeChannel=config.modeChannel)
opti = tf.keras.optimizers.Adam(learning_rate=config.learning_rate)
model.compile(opti, loss=[lambda x,y:lossFGWithReject(x,y,config.lambdahyper),lossHAux], loss_weights=[config.weightLoss1,1-config.weightLoss1],
              metrics=[metrics,[]])

#%% md

### Prepare input data

#%%

def generatorData(pathPrepro:str, filesList: List[str]):
    for file in filesList:
        data = np.load(pathPrepro + separator + file)
        # data = tf.cast(data,tf.float32)
        label = [int(file.split("_")[1].split(".")[0])]*len(data) # we repeat the label
        yield data, tf.expand_dims(label,axis=-1)

generatorDataTrain = lambda  : generatorData(pathPrepro=pathPreprocessedData+"Train",filesList=train)
generatorDataValid = lambda  : generatorData(pathPrepro=pathPreprocessedData+"Train",filesList=valid)

# masker = tf.keras.layers.Masking(mask_value=-2.)
# def maskingGT(input1, input2):
#     # input1: [Batch,#segments,X,Y,1]
#     # input2: [Batch,#segments,1]
#     theMask = masker.compute_mask(input2) # theMask: [Batch, #segments]
#     input2 = tf.boolean_mask(input2, theMask) # [batch*segments-masked]
#     input2 = tf.reshape(input2,[-1]) # [batch*segments-masked,]
#     input2 = tf.one_hot(tf.cast(input2,tf.int32),nbClass) #[batch*segments-masked,nbClass]
#     return input1,input2

def repeatGT(input1, input2):
    #input 2 : [batch seq 1]
    input2 = tf.reshape(input2,[tf.shape(input2)[0],tf.shape(input2)[1]])
    input2 = tf.one_hot(tf.cast(input2,tf.int32),nbClass) #[batch,segments,nbClass]
    return input1,(input2,input2)

def getDataset(generator,size):
    output_shapes = (tf.TensorShape([None, dimensionsImage[0], dimensionsImage[1], canal]),
                                       tf.TensorShape([None, 1]))
    dataset = tf.data.Dataset.from_generator(
                        generator,
                        output_types=(tf.float32, tf.float32),
                        output_shapes=output_shapes
                    )
    toPad = ((tf.constant(0.)), tf.constant(0.))
    dataset = dataset.shuffle(buffer_size=size, reshuffle_each_iteration=True)
    dataset = dataset.padded_batch(config.batchSize, padded_shapes=output_shapes,
                                           padding_values=toPad)

    # dataset = dataset.map(maskingGT) # masking
    dataset = dataset.map(repeatGT) # repeat the GT + one hot encoding
    dataset = dataset.repeat()
    return dataset


datasetTrain = getDataset(generatorDataTrain,nbTrain)
datasetValid = getDataset(generatorDataValid,nbValid)

#%% md

### Prepare callbacks for training

#%%

date=datetime.now().strftime("%Y%m%d-%H%M%S")
pathWeight = pathLog+date+separator+"Weights"
if not os.path.exists(pathLog):
    os.mkdir(pathLog)
os.mkdir(pathLog+date)
os.mkdir(pathWeight)
os.mkdir(pathLog+date+separator+"TensorBoard")
earlyStop = tf.keras.callbacks.EarlyStopping(monitor="val_output_1_TAR_allValues", verbose=1, patience=70, mode='max')
checkpoint =  tf.keras.callbacks.ModelCheckpoint(pathLog+date+separator+"Weights"+separator+"model", monitor="val_output_1_TAR_allValues", verbose=1, save_best_only=True,
                                     mode='max')
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=pathLog+ date+separator+"TensorBoard"+separator+"training", histogram_freq=1)
callbacks = [earlyStop,checkpoint,tensorboard_callback,WandbCallback()]



#%% md

# Fitting

#%%

history = model.fit(datasetTrain, epochs=3000, steps_per_epoch=nbTrain / config.batchSize, verbose=2,
                        validation_data=datasetValid, validation_steps= nbValid / config.batchSize,
                        callbacks=callbacks)  # val_size / batchSize
print("fitted ! ", len(history.history['loss']), " epochs")

#%% md

### add some values in wandb

#%%

import tensorflow.keras.backend as K
trainable_count = np.sum([K.count_params(w) for w in model.trainable_weights])
wandb.log({"TrainableParams":trainable_count})
try:
    def myprint(s):
        print(s)
        with open(pathWeight+separator+"totalWeigth.txt", 'a+') as f:
            f.write(s+"\n")

    model.summary(print_fn=myprint)
except Exception as e:
    print("Problem with weight calculation 2")
    print(e)
# copyfile(ModelEarly2D.py,wandBRunDir+"ModelEarly2D.py")
wandb.finish()

#%%



